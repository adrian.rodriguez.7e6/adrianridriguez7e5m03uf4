﻿using System;
using System.Drawing;

namespace P1M03Uf4AdrianRodriguez
{
    public class Programa
    {
        public abstract class FiguraGeometrica
        {
            protected int codi { get; set; }
            protected string nom { get; set; }
            protected Color color { get; set; }

            protected FiguraGeometrica()
            {
                this.codi = 1;
                this.nom = "Name default";
                this.color = new Color();
            }

            protected FiguraGeometrica(int Codi, string Nom, Color Color)
            {
                this.codi = Codi;
                this.nom = Nom;
                this.color = Color;
            }

            public abstract double Area();

            public override bool Equals(object obj)
            {
                FiguraGeometrica Id=(FiguraGeometrica)obj;
                if (this.codi == Id.codi) return true;
                else return false;
            }

            public override int GetHashCode()
            {
                
                return this.codi%2;
            }
        }

        class Rectangle : FiguraGeometrica
        {
            public double longitud { get; set; }
            public double altura { get; set; }

            public Rectangle()
            {
                this.longitud = 1;
                this.altura = 1;
                this.codi= 1;
            }

            public Rectangle(double Longitud, double Altura)
            {
                this.longitud = Longitud;
                altura = Altura;

            }

            public string toString()
            {
                return $"Esta figura es un rectangulo de base {longitud} y de altura {altura}";
            }

            public double Perimetro(double longitud, double altura)
            {
                return 2 * longitud + 2 * altura;
            }

            public override double Area()
            {
                return this.longitud * this.altura;
            }
        }

        class Triangle : FiguraGeometrica
        {
            public double longitud { get; set; }
            public double altura { get; set; }

            public Triangle()
            {
                this.longitud = 1;
                this.altura = 1;
                this.codi= 2;
            }

            public Triangle(double Longitud, double Altura)
            {
                this.longitud = Longitud;
                this.altura = Altura;
            }
            public string toString()
            {
                return $"Esta figura es un triangulo de base {longitud} y de altura {altura}";
            }

            public double Perimetro(double longitud, double altura)
            {
                double hipotenusa = Math.Sqrt(Math.Pow(longitud / 2, 2) + Math.Pow(altura, 2));
                return longitud + 2*hipotenusa;
            }

            public override double Area()
            {
                return (longitud * altura)/2;
            }
        }

        class Cercle : FiguraGeometrica
        {
            public double radio { get; set; }

            public Cercle()
            {
                this.radio = 1;
                this.codi = 3;
            }

            public Cercle(double Radio)
            {
                this.radio = Radio;
            }
            public string toString()
            {
                return $"Esta figura es un circulo de radio {radio}";
            }

            public double Perimetro(double radio)
            {
                return 2 * Math.PI * radio;
            }

            public override double Area()
            {
                return Math.PI * Math.Pow(radio, 2);
            }
        }

        class ProvaFigures
        {
            static void Main(string[] args)
            {
                Console.WriteLine("Rectangulo");
                Rectangle rgl = new Rectangle();
                Console.WriteLine(rgl.altura);
                Console.WriteLine(rgl.longitud);
                Console.WriteLine(rgl.toString());
                Console.WriteLine(rgl.Area());
                Console.WriteLine(rgl.GetHashCode());

                Console.WriteLine("\n\nTriangulo");
                Triangle trl = new Triangle();
                Console.WriteLine(trl.altura);
                Console.WriteLine(trl.longitud);
                Console.WriteLine(trl.toString());
                Console.WriteLine(trl.Area());
                Console.WriteLine(trl.GetHashCode());

                Console.WriteLine("\n\nCirculo");
                Cercle cle = new Cercle();
                Console.WriteLine(cle.radio);
                Console.WriteLine(cle.toString());
                Console.WriteLine(cle.Area());
                Console.WriteLine(cle.GetHashCode());
                

                Console.WriteLine("\nCOMPARACIONES");
                Console.WriteLine($"Rectangle amb triangle: {rgl.Equals(trl)}");
                Console.WriteLine($"Rectangle amb cercle: {rgl.Equals(cle)}");
                Console.WriteLine($"Triangle amb cercle: {trl.Equals(cle)}");
            }
        }
    }
}
